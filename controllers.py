from app import  app, mail, log, ALLOWED_EXTENSIONS,  REGISTRATION_LINK, FORGER_PASSWORD_LINK, APP_MAIL
from .models import *
from .enums import OfferStatus
from flask_mail import Message
from sqlalchemy.orm import Query
from sqlalchemy import or_, and_
from hashlib import md5, sha256
import os



class DatabaseController():

    def __init__(self):
        pass

#---------------------GENERAL-----------------------------------
    def __password_generator(self, email, password):
        end = len(email)
        start = end - len(email.split('@')[0])
        return sha256(md5(password.encode()).digest()).hexdigest()[start:end]


    def check_mail_exist(self, email):
        try:
            log.info('{} - checked mail existence')
            return Users.query.filter_by(email=email).first() is not None
        except Exception as e:
            print(e)
        return None


    def check_user_login(self, email, password):
        try:
            hash_password = self.__password_generator(email=email ,password=password)
            user = Users.query.filter_by(email=email, password=hash_password).first()
            if (user is not None) and user.is_activated:
                return (True, user.user_type)
            else:
                return (False, None)
        except Exception as e:
            print(e)

        return (False, None)


    def generate_new_user(self, email, password, activation_code):
        hash_password = self.__password_generator(email=email, password=password)

        if self.check_mail_exist(email):
            return False
        try:
            user = Users(email=email, password=hash_password, activation_code=activation_code)
            database.session.add(user)
            database.session.commit()
            log.info('{} - new user is generated'.format(email))
            return True
        except Exception as e:
            log.error('{} - user generation error'.format(email))
            print(e)

        return False

    @staticmethod
    def get_registration_code(email):
        try:
            return Users.query.filter_by(email=email).first().activation_code[::-1]
        except Exception as e:
            print(e)
        return False

    def activate_accout(self, activation_code):
        try:
            Users.query.filter_by(activation_code=activation_code).update(dict(is_activated=True))
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False

    def change_password(self, activation_code, password):
        try:
            activation_code = activation_code[::-1]
            email = Users.query.filter_by(activation_code=activation_code).first().email
            Users.query.filter_by(activation_code=activation_code).update(dict(password=self.__password_generator(email=email, password=password)))
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False

    def verify_by_activation_code(self, activation_code):
        activation_code = activation_code[::-1]
        try:
            user = Users.query.filter_by(activation_code=activation_code).first()
            return user is not None
        except Exception as e:
            print(e)
        return False

    def comments(self):
        try:
            comments = Evalauation.query.all()
            return dict(comments=comments)
        except Exception as e:
            print(e)
        return False

#-------------------------------------------------------------------


#--------------------------ADMIN------------------------------------


    def admin_requests(self):
        try:
            requests = Offers.query.filter(or_(Offers.status == OfferStatus.APPROVED, Offers.status == OfferStatus.INFO)).all()
            # requests = Offers.query.filter(and_(Offers.status != OfferStatus.DONE, Offers.status != OfferStatus.CANCELLED)).all()
            return dict(requests=requests)
        except Exception as e:
            print(e)
        return None


    def admin_menu_statistics(self):
        try:
            messages = Messages.query.filter(and_(Messages.receiver == 'admin', Messages.visibility == 1)).all()
            requests = Offers.query.filter(or_(Offers.status == OfferStatus.APPROVED, Offers.status == OfferStatus.INFO)).all()
            #requests = Offers.query.filter(and_(Offers.status != OfferStatus.DONE, Offers.status != OfferStatus.CANCELLED)).all()
            return dict(request_count=len(requests), message_count=len(messages))
        except Exception as e:
            print(e)
        return None


    def admin_send_message(self, sender, receiver, topic, message):
        try:
            message = Messages(sender=sender, receiver=receiver, topic=topic, message=message)
            database.session.add(message)
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False


    def admin_get_messages(self):
        try:
            messages = Messages.query.filter(and_(Messages.receiver == 'admin', Messages.visibility == 1)).all()
            return dict(messages=messages)
        except Exception as e:
            print(e)
        return None


    def admin_delete_message(self, message_id):
        try:
            Messages.query.filter_by(id=message_id).update(dict(visibility=0))
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False


    def admin_update_offer(self, request_id, offer):
        try:
            Offers.query.filter_by(id=request_id).update(dict(offer=offer))
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False

    def admin_update_offer_status(self, request_id, status):
        try:
            Offers.query.filter_by(id=request_id).update(dict(status=status))
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False

    def admin_upload_file(self, request_id, file):
        try:
            Offers.query.filter_by(id=request_id).update(dict(solution_file=file))
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False
#--------------------------------------------------------------------



#---------------------------------CUSTOMER---------------------------

    def customer_menu_statistics(self, email):
        try:
            messages = Messages.query.filter_by(receiver=email).all()
            requests = Offers.query.filter_by(customer=email).all()

            return dict(request_count=len(requests), message_count=len(messages))
        except Exception as e:
            print(e)
        return None


    def customer_requests(self, customer):
        try:
            requests = Offers.query.filter_by(customer=customer).all()
            return requests[::-1]
        except Exception as e:
            print('customer_request() -> {}'.format(e))
        return None


    def add_new_request(self, customer, course, file, note):

        try:
            request = Offers(customer=customer, course=course, file=file, note=note)
            database.session.add(request)
            database.session.commit()
            log.info('New Offer: Customer: {} - Course: {} - File: {}'.format(customer, course, file))
            return True
        except Exception as e:
            print('add_new_request() -> {}'.format(e))
        return False


    def update_request_status(self, customer, request_id, offerstatus):
        try:
            Offers.query.filter_by(customer=customer, id=request_id).update(dict(status = offerstatus))
            database.session.commit()
            return True
        except Exception as e:
            print(e)

        return False


    def customer_send_message(self, sender, topic, message):
        try:
            message = Messages(sender=sender, topic=topic, message=message)
            database.session.add(message)
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False


    def customer_get_message(self, receiver):
        try:
            messages = Messages.query.filter_by(receiver=receiver).all()
            messages.extend(Messages.query.filter_by(sender=receiver).all())
            return messages
        except Exception as e:
            print(e)
        return False

    def add_new_evaluation(self, customer, request_id, comment, score):
        try:
            evaluation = Evalauation(customer=customer, request_id=request_id, comment=comment, score=score)
            database.session.add(evaluation)
            database.session.commit()
            return True
        except Exception as e:
            print(e)
        return False


#-------------------------------------------------------------------



class MainController:

    def __init__(self):
        pass

    def generate_registration_code(self, email):
        return sha256(md5(email.encode()).digest()).hexdigest()

    def allowed_file(self, filename):
        return '.' in filename and filename.split('.')[1] in ALLOWED_EXTENSIONS

    def upload_file_to_server(self, file):
        try:
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
            return True, ('../{}{}'.format(app.config['DOWNLOAD_FOLDER'], file.filename))
        except Exception as e:
            print(e)
        return False, None


class MailController:

    def __init__(self):
        pass


    def send_registration_mail(self, receiver, activation_code):
        title = 'dmcsh registration'

        message = 'Please click this link \n{}{}\nto complate your registration to dmcsh'.format(REGISTRATION_LINK,activation_code, REGISTRATION_LINK,activation_code)
        msg = Message(title, sender=APP_MAIL, recipients=[receiver], body=message)
        mail.send(msg)
        return True


    def send_forget_password_mail(self, receiver):
        title = 'dmcsh password'
        message = 'Please click this link \n{}{}\nto change your registration to dmcsh'.format(FORGER_PASSWORD_LINK, DatabaseController.get_registration_code(email=receiver))
        msg = Message(title, sender=APP_MAIL, recipients=[receiver], body=message)
        mail.send(msg)
        return True

    def send_notification_mail(self):
        title = ''
