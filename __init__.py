from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
import logging


UPLOAD_FOLDER = '/home/umitkas/UMITKAS/Projects/ProjectDMCSH/app/static/download/'
DOWNLOAD_FOLDER = 'static/download/'
ALLOWED_EXTENSIONS = set(['word','pdf', 'zip'])
REGISTRATION_LINK = 'dmcsh.pythonanywhere.com/accountactivation/'
FORGER_PASSWORD_LINK = 'dmcsh.pythonanywhere.com/forgetpassword/'
APP_MAIL = 'http://www.dmcshm@gmail.com'


app = Flask(__name__)

app.secret_key = '35e0ef089a33d723e6a419c64d4b47a4'
#-------------------------FILE CONFIG-------------------------
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
#-------------------------------------------------------------

#-----------------------DATABASE CONFIG-----------------------
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://///home/umitkas/UMITKAS/Projects/ProjectDMCSH/db/database.db'
#-------------------------------------------------------------

#---------------------MAIL CONFIG----------------------------
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'dmcshm@gmail.com'
app.config['MAIL_PASSWORD'] = 'xHusmkas.1995'
app.config['MAIL_USE_SSL'] = True




#-------------------------------------------------------------



#---------------------EXTRA CONFIG----------------------------

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
#-------------------------------------------------------------




#------------------------GLOBAL OBJECTS------------------------
database = SQLAlchemy(app)
mail = Mail(app)
log = logging

#-------------------------------------------------------------


from app import views, models, controllers



#log.basicConfig(filename='/home/umitkas/UMITKAS/Projects/ProjectDMCSH/db/system.log', filemode='w', level=log.DEBUG)


# ---------------------------
database.create_all()


