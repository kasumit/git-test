from app import database

class Users(database.Model):
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    email = database.Column(database.String(100), unique=True)
    nick = database.Column(database.String(100), unique=True)
    password = database.Column(database.String(100))
    status = database.Column(database.Boolean, default=True)
    user_type = database.Column(database.CHAR, default=1)
    is_activated = database.Column(database.Boolean, default=False)
    activation_code = database.Column(database.String(250))

    def __init__(self, email, password, activation_code):
        self.email = email
        self.nick = email.split('@')[0]
        self.password = password
        self.activation_code = activation_code

    def __repr__(self):
        return 'mail: {} - hash_pass: {} - nick: {}'.format(self.email, self.password, self.nick)



class Messages(database.Model):

    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    sender = database.Column(database.String(100))
    receiver = database.Column(database.String(100))
    sender_nick = database.Column(database.String(100))
    topic = database.Column(database.String(200))
    message = database.Column(database.Text(400))
    date = database.Column(database.DATETIME, server_default= database.func.now(), onupdate=database.func.now())
    visibility = database.Column(database.Boolean, default=True)


    def __init__(self, sender, topic, message, receiver='admin'):
        self.sender = sender
        self.receiver = receiver
        self.sender_nick = Users.query.filter_by(email=sender).first().nick
        self.topic = topic
        self.message = message



    def __repr__(self):
        return '{} to {} - {} -- {}'.format(self.sender, self.receiver, self.topic, self.message)


class Offers(database.Model):
    '''
        STATUS 4 APPROVED
        STATUS 3 CANCEL
        STATUS 2 DONE
        STATUS 1 INFO

    '''
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    customer = database.Column(database.String(100))
    nick = database.Column(database.String(100))
    course = database.Column(database.String(100))
    file = database.Column(database.String(250))
    solution_file = database.Column(database.String(250))
    note = database.Column(database.String(250))
    offer = database.Column(database.Float, default= 0.0)
    status = database.Column(database.Integer, default=1)

    date = database.Column(database.DATETIME, server_default=database.func.now(), onupdate=database.func.now())

    def __init__(self, customer, course, file, note):
        self.customer = customer
        self.course = course
        self.file = file
        self.note = note
        self.nick = Users.query.filter_by(email=customer).first().nick

    def __repr__(self):
        return '{} - {}'.format(self.customer, self.course)


class Evalauation(database.Model):
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    request_id = database.Column(database.Integer)
    customer = database.Column(database.String(100))
    comment = database.Column(database.String(500))
    date = database.Column(database.DATETIME, server_default=database.func.now())
    score = database.Column(database.Integer)
    course = database.Column(database.String(50))
    language = database.Column(database.String(50))

    def __init__(self, customer, request_id, comment, score):
        self.request_id = request_id
        self.customer = customer
        self.comment = comment
        self.score = score
        request = Offers.query.filter_by(id=request_id).first()
        self.course = request.course
        self.language = request.note

    def __repr__(self):
        return '{} - {} - {}'.format(self.customer, self.course, self.score)
